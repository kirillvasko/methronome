package com.kirillvasko.methronome

import android.media.AudioAttributes
import android.media.AudioFormat
import android.media.AudioTrack

object Metronome {
    private const val MILLISECONDS_IN_SECOND: Int = 1000
    private const val SECONDS_IN_MINUTE: Int = 60

    private var isPlaying: Boolean = false
    private lateinit var audioTrack: AudioTrack

    fun start(bpm: Int, frequency: Float): Boolean {
        if (this.isPlaying) {
            return false
        }
        this.isPlaying = true
        generateSound(bpm, frequency)
        return true
    }

    fun changeBpm(bpm: Int, frequency: Float): Boolean {
        if (!this.isPlaying) {
            return false
        }
        generateSound(bpm, frequency)
        return true
    }

    fun stop(): Boolean {
        if (!this.isPlaying) {
            return false
        }
        this.isPlaying = false
        this.audioTrack.stop()
        return true
    }

    fun isOn(): Boolean {
        return this.isPlaying
    }

    fun generateSound(bpm: Int, frequency: Float) {
        if (this::audioTrack.isInitialized) {
            this.audioTrack.stop()
        }
        val sampleRate = 44100
        val beatDurationInMilliseconds = 2 // This is the duration of the sound in milliseconds
        val beatSamples = (sampleRate * beatDurationInMilliseconds / 1000).toInt() // Convert to samples
        val silenceDurationInMilliseconds = (MILLISECONDS_IN_SECOND * SECONDS_IN_MINUTE / bpm).toLong() - beatDurationInMilliseconds
        val silenceSamples = (sampleRate * silenceDurationInMilliseconds / 1000).toInt() // Convert to samples
        val totalSamples = beatSamples + silenceSamples
        val buffer = ShortArray(totalSamples)

        // Define the peak volume (75% volume)
        val peakVolume = (Short.MAX_VALUE * 0.75).toInt()

        // Fill the buffer with samples of a sine wave at the desired frequency for the beat duration
        for (i in 0 until beatSamples) {
            buffer[i] = (peakVolume * Math.sin(2.0 * Math.PI * i.toDouble() / (sampleRate / frequency))).toInt().toShort()
        }

        // Define the number of samples over which the fade-out should occur
        val fadeOutSamples = beatSamples / 10 // Fade out over the last 10% of the beat

        // Apply a linear fade-out to the end of the beat
        for (i in beatSamples - fadeOutSamples until beatSamples) {
            val fadeOutFactor = (beatSamples - i).toFloat() / fadeOutSamples
            buffer[i] = (buffer[i] * fadeOutFactor).toInt().toShort()
        }

        // Fill the rest of the buffer with silence
        for (i in beatSamples until totalSamples) {
            buffer[i] = 0
        }

        val audioAttributes = AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_MEDIA)
            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
            .build()

        val audioFormat = AudioFormat.Builder()
            .setSampleRate(sampleRate)
            .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
            .setChannelMask(AudioFormat.CHANNEL_OUT_MONO)
            .build()

        this.audioTrack = AudioTrack.Builder()
            .setAudioAttributes(audioAttributes)
            .setAudioFormat(audioFormat)
            .setBufferSizeInBytes(totalSamples * 2)
            .setTransferMode(AudioTrack.MODE_STATIC)
            .build()

        this.audioTrack.write(buffer, 0, totalSamples)
        this.audioTrack.setLoopPoints(0, totalSamples, -1) // Set the audio track to loop indefinitely
        this.audioTrack.play()
    }
}