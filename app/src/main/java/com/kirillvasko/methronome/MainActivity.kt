package com.kirillvasko.methronome

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.kirillvasko.methronome.ui.theme.MethronomeTheme
import kotlin.math.roundToInt

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MethronomeTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background,
                ) {
                    val bpm = remember { mutableStateOf(60f) }
                    val button_text = remember { mutableStateOf("Start") }
                    val frequency = remember { mutableStateOf(440f) } // Initial frequency for A4 note

                    Box(
                        modifier = Modifier.fillMaxSize(),
                        contentAlignment = Alignment.Center
                    ) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.spacedBy(16.dp)
                        ) {
                            TextField(
                                value = String.format("%.0f", bpm.value),
                                onValueChange = {
                                    val newValue = it.toFloatOrNull()
                                    bpm.value = if (newValue != null && newValue > 0) newValue else 60f // Default value is 60
                                },
                                label = { Text("BPM") },
                                modifier = Modifier.fillMaxWidth(0.5f)
                            )
                            Row(
                                horizontalArrangement = Arrangement.Center,
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                Button(
                                    onClick = {
                                        val newValue = ((bpm.value - 5) / 5).roundToInt() * 5
                                        bpm.value = newValue.toFloat()
                                        Metronome.changeBpm(bpm.value.toInt(), frequency.value)
                                    },
                                    modifier = Modifier.size(70.dp, 50.dp),
                                    shape = RoundedCornerShape(32.dp)
                                ) {
                                    Text(text = "-5")
                                }

                                Slider(
                                    value = bpm.value,
                                    onValueChange = { bpm.value = it },
                                    onValueChangeFinished = { Metronome.changeBpm(bpm.value.toInt(), frequency.value) },
                                    valueRange = 40f..240f, // BPM range from 40 to 240
                                    modifier = Modifier.fillMaxWidth(0.5f)
                                )

                                Button(
                                    onClick = {
                                        val newValue = ((bpm.value + 5) / 5).roundToInt() * 5
                                        bpm.value = newValue.toFloat()
                                        Metronome.changeBpm(bpm.value.toInt(), frequency.value)
                                    },
                                    modifier = Modifier.size(70.dp, 50.dp),
                                    shape = RoundedCornerShape(32.dp)
                                ) {
                                    Text(text = "+5")
                                }
                            }
                            Text(text = String.format("%.0f", bpm.value))
                            Row(
                                horizontalArrangement = Arrangement.Center,
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                Button(
                                    onClick = {
                                        val newValue = ((frequency.value - 5) / 5).roundToInt() * 5
                                        frequency.value = newValue.toFloat()
                                        Metronome.changeBpm(bpm.value.toInt(), frequency.value)
                                    },
                                    modifier = Modifier.size(70.dp, 50.dp),
                                    shape = RoundedCornerShape(32.dp)
                                ) {
                                    Text(text = "-5")
                                }

                                Slider(
                                    value = frequency.value,
                                    onValueChange = { frequency.value = it },
                                    onValueChangeFinished = { Metronome.changeBpm(bpm.value.toInt(), frequency.value) },
                                    valueRange = 20f..1000f, // Frequency range from 20 Hz to 1,000 Hz
                                    modifier = Modifier.fillMaxWidth(0.5f)
                                )

                                Button(
                                    onClick = {
                                        val newValue = ((frequency.value + 5) / 5).roundToInt() * 5
                                        frequency.value = newValue.toFloat()
                                        Metronome.changeBpm(bpm.value.toInt(), frequency.value)
                                    },
                                    modifier = Modifier.size(70.dp, 50.dp),
                                    shape = RoundedCornerShape(32.dp)
                                ) {
                                    Text(text = "+5")
                                }
                            }
                            Text(text = String.format("%.2f", frequency.value))
                            Button(
                                onClick = {
                                    if (Metronome.isOn()) {
                                        Metronome.stop()
                                        button_text.value = "Start"
                                    } else {
                                        Metronome.start(bpm.value.toInt(), frequency.value)
                                        button_text.value = "Stop"
                                    }
                                },
                                modifier = Modifier.size(200.dp, 50.dp),
                                shape = RoundedCornerShape(32.dp)
                            ) {
                                Text(text = button_text.value)
                            }
                        }
                    }
                }
            }
        }
    }
}